include(src/Dolly.pri)

TEMPLATE = app
TARGET = dolly

CONFIG += console c++11
CONFIG -= app_bundle
QT += core gui widgets
QMAKE_CXXFLAGS += -std=c++11 -O2

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

DISTFILES += \
    .gitignore

RESOURCES += \
    dolly.qrc
