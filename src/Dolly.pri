HEADERS += \
    $$PWD/bonewidget.h \
    $$PWD/garnish.h \
    $$PWD/project/project.h \
    $$PWD/project/projectwizard.h \
    $$PWD/skeletonwidget.h \
    $$PWD/resourcebrowser.h \
    $$PWD/weaver.h \
    $$PWD/qocoon.h \
    $$PWD/view3d.h \
    $$PWD/dry.h \
    $$PWD/jib.h \
    $$PWD/drywidget.h \
    $$PWD/drydockwidget.h

SOURCES += \
    $$PWD/bonewidget.cpp \
    $$PWD/garnish.cpp \
    $$PWD/main.cpp \
    $$PWD/project/project.cpp \
    $$PWD/project/projectwizard.cpp \
    $$PWD/skeletonwidget.cpp \
    $$PWD/resourcebrowser.cpp \
    $$PWD/weaver.cpp \
    $$PWD/qocoon.cpp \
    $$PWD/view3d.cpp \
    $$PWD/jib.cpp \
    $$PWD/drywidget.cpp \
    $$PWD/drydockwidget.cpp
