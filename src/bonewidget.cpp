/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>

#include "weaver.h"
#include "qocoon.h"

#include "bonewidget.h"

BoneWidget::BoneWidget(Qocoon* parent): DryWidget(parent->GetContext(), parent),
    nameLabel_{ new QLabel{ "Name" } },
    rigidButton_{      new QPushButton{ QIcon{ ":/Down" }, "Rigid Body" } },
    colliderButton_{   new QPushButton{ QIcon{ ":/Down" }, "Collider" } },
    constraintButton_{ new QPushButton{ QIcon{ ":/Down" }, "Constraint" } },
    rigidScroll_{      new QScrollArea{} },
    colliderScroll_{   new QScrollArea{} },
    constraintScroll_{ new QScrollArea{} },
    spacer_{ SPACER },
    bone_{ nullptr }
{
    setObjectName("Bone");

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setMargin(0);

    nameLabel_->setEnabled(false);
    mainLayout->addWidget(nameLabel_);

    const QString buttonStyle{ "text-align: left; padding-left: 2px; outline: none;" };

    mainLayout->addWidget(rigidButton_);
    mainLayout->addWidget(rigidScroll_);

    mainLayout->addWidget(colliderButton_);
    mainLayout->addWidget(colliderScroll_);

    mainLayout->addWidget(constraintButton_);
    mainLayout->addWidget(constraintScroll_);

    spacer_->setVisible(false);
    mainLayout->addWidget(spacer_);

    setLayout(mainLayout);

    for (auto b: { rigidButton_, colliderButton_, constraintButton_ })
    {
        b->setStyleSheet(buttonStyle);
        b->setFlat(true);
        connect(b, SIGNAL(clicked()), this, SLOT(collapse()));
    }

    for (auto s: { rigidScroll_, colliderScroll_, constraintScroll_ })
    {
        s->setMinimumHeight(040);
    }
}

void BoneWidget::collapse()
{
    if (sender() == rigidButton_)
    {
        const bool visible{ rigidScroll_->isVisible() };
        rigidScroll_->setVisible(!visible);
        rigidButton_->setIcon(QIcon{ (visible ? ":/Right" : ":/Down") });
    }
    else if (sender() == colliderButton_)
    {
        const bool visible{ colliderScroll_->isVisible() };
        colliderScroll_->setVisible(!visible);
        colliderButton_->setIcon(QIcon{ (visible ? ":/Right" : ":/Down") });
    }
    else
    {
        const bool visible{ constraintScroll_->isVisible() };
        constraintScroll_->setVisible(!visible);
        constraintButton_->setIcon(QIcon{ (visible ? ":/Right" : ":/Down") });
    }

    spacer_->setVisible(!rigidScroll_->isVisible()    &&
                        !colliderScroll_->isVisible() &&
                        !constraintScroll_->isVisible());
}

void BoneWidget::setBone(Node* bone)
{
    if (bone)
    {
        nameLabel_->setText(toQString(bone->GetName()));
        nameLabel_->setEnabled(true);
    }
    else
    {
        nameLabel_->setText("Name");
        nameLabel_->setEnabled(false);
    }
}
