/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BONEWIDGET_H
#define BONEWIDGET_H

#include <QLabel>
#include <QPushButton>
#include <QScrollArea>

#include "drywidget.h"

class Qocoon;

class BoneWidget: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(BoneWidget, DryWidget)

public:
    explicit BoneWidget(Qocoon* parent);

public slots:
    void setBone(Node* bone);

private slots:
    void collapse();

private:
    QLabel*      nameLabel_;
    QPushButton* rigidButton_;
    QPushButton* colliderButton_;
    QPushButton* constraintButton_;
    QScrollArea* rigidScroll_;
    QScrollArea* colliderScroll_;
    QScrollArea* constraintScroll_;
    QWidget*     spacer_;

    Node* bone_;
};

#endif // BONEWIDGET_H
