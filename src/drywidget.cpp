/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "drywidget.h"

DryWidget::DryWidget(Context* context, QWidget *parent): QWidget(parent), Object(context),
    orientation_{ Qt::Vertical }
{

}

bool DryWidget::setOrientation(Qt::Orientation orientation)
{
    if (orientation_ == orientation)
        return false;

    orientation_ = orientation;

    return true;
}

FlipSplitter::FlipSplitter(QWidget* parent, int snap): QSplitter(parent),
    threshold_{ 1.0f },
    squareSnap_{ snap }
{
//    if (squareSnap_ != -1)
//        connect(this, SIGNAL(splitterMoved(int, int)), SLOT(snap()));
}

void FlipSplitter::setStretchFactors(Qt::Orientation orientation, int first, int second)
{
    stretchFactors_[orientation] = std::pair<int, int>{ first, second };

    if (QSplitter::orientation() == orientation)
    {
        setStretchFactor(0, stretchFactors_[orientation].first);
        setStretchFactor(1, stretchFactors_[orientation].second);
    }
}

void FlipSplitter::resizeEvent(QResizeEvent* event)
{
    float ratio{ static_cast<float>(width()) / height() };
//    double bias{ static_cast<double>(sizes()[0] + sizes()[1]) / sizes()[1] };
//    ratio -= bias * 0.1;

    Qt::Orientation splitterOrientation{ orientation() };

    if (ratio > threshold_ + 0.1f)
        splitterOrientation = Qt::Horizontal;
    else if (ratio < threshold_ - 0.1f)
        splitterOrientation = Qt::Vertical;

    if (orientation() != splitterOrientation)
    {
        setOrientation(splitterOrientation);
        setStretchFactor(0, stretchFactors_[splitterOrientation].first);
        setStretchFactor(1, stretchFactors_[splitterOrientation].second);

        emit orientationChanged(splitterOrientation);
    }

    QSplitter::resizeEvent(event);
}
