/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FILEWIDGET_H
#define FILEWIDGET_H

#include "dry.h"

#include <QSplitter>
#include <QWidget>

#define TAG_DEFAULT "DEFAULT"
#define SPACER [&](){ QWidget* spacer{ new QWidget() }; spacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding); return spacer; }()

class DryWidget: public QWidget, public Object
{
    Q_OBJECT
    DRY_OBJECT(DryWidget, Object)

public:
    explicit DryWidget(Context* context, QWidget *parent = nullptr);

    virtual bool setOrientation(Qt::Orientation orientation);

protected:
    Qt::Orientation orientation_;
};

class FlipSplitter: public QSplitter
{
    Q_OBJECT

public:
    FlipSplitter(QWidget* parent = nullptr, int snap = -1);
    void setStretchFactors(Qt::Orientation orientation, int first, int second);
    void setThreshold(double threshold) { threshold_ = threshold; }

signals:
    void orientationChanged(Qt::Orientation);

public slots:
    void resizeEvent(QResizeEvent* event) override;

private:
    std::map<Qt::Orientation, std::pair<int, int>> stretchFactors_;
    float threshold_;
    const int squareSnap_;
};

#endif // FILEWIDGET_H
