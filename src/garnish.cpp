/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPainter>
#include <QApplication>
#include "view3d.h"
#include "skeletonwidget.h"

#include "garnish.h"

Garnish::Garnish(View3D* parent): QObject(parent),
    pixmap_{ parent->width(), parent->height() },
    view_{ parent }
{
    pixmap_.fill(Qt::transparent);
}

void Garnish::update(SkeletonWidget* skeletonWidget)
{
    pixmap_ = QPixmap{ view_->size() };
    pixmap_.fill(Qt::transparent);
    if (!skeletonWidget)
        return;

    AnimatedModel* model{ skeletonWidget->modelComponent() };
    if (!model || !model->GetModel())
        return;

    const PODVector<unsigned> selection{ skeletonWidget->boneIndices(true) };
    const Skeleton& skeleton{ model->GetSkeleton() };
    Jib* jib{ view_->jib() };

    QPainter p{};
    p.begin(&pixmap_);
    p.setRenderHint(QPainter::Antialiasing);

    for (const Bone& b: skeletonWidget->bones())
    {
        const Vector3 boneDirection{ b.node_->GetWorldUp() };
        const Vector3 worldPos{ b.node_->GetWorldPosition() };
        const Vector2 nBonePos{ jib->WorldToScreenPos(worldPos) };
        const QPointF bonePos{ view_->width()  * nBonePos.x_,
                               view_->height() * nBonePos.y_ };

        const Vector2 nBoneTip{ jib->WorldToScreenPos(worldPos + boneDirection * b.boundingBox_.max_.y_) };
        const QPointF boneTip{ view_->width()  * nBoneTip.x_,
                               view_->height() * nBoneTip.y_ };

        const unsigned index{ skeleton.GetBoneIndex(&b) };
        if (index == skeletonWidget->currentBoneIndex())
            p.setOpacity(1.);
        else
            p.setOpacity(.5);

        const float boneDistance{ jib->GetNode()->GetWorldPosition().DistanceToPoint(worldPos) * jib->GetFov() };
        const float s{ 1000.f / boneDistance };
        QColor highlight{ QApplication::palette().color(QPalette::Highlight) };
        p.setPen(QPen{ Qt::white, s, Qt::SolidLine, Qt::RoundCap });
        p.drawLine(bonePos, boneTip);
        p.setPen(QPen{ highlight, s - 2, Qt::SolidLine, Qt::RoundCap });
        p.drawLine(bonePos, boneTip);

        if (selection.Contains(index))
        {
            p.setBrush(highlight);
            highlight.setHsv(highlight.hsvHue(), 255, 255);
            p.setPen({ highlight, 2 });
        }
        else
        {
            p.setBrush(Qt::gray);
            p.setPen(Qt::black);
        }

        p.drawEllipse(bonePos, s, s);
    }

    p.end();
}
