#include "../weaver.h"

#include "project.h"


Project::Project():
    context_{ MakeShared<Context>() }
{
    context_->RegisterSubsystem<FileSystem>();
    context_->RegisterSubsystem<ResourceCache>();
}

Project::Project(XMLFile* file)
{
    read(file);
}

bool Project::read(XMLFile* file)
{
    if (!file)
        return  false;

    if (const XMLElement luckeyElem{ file->GetRoot("luckey") })
    {
        if (const XMLElement projectElem{ luckeyElem.GetChild("project") })
        {
            const std::string fullFileName{ file->GetName().CString() };
            std::string fileName{ fullFileName.substr(fullFileName.find_last_of('/') + 1) };

            info_.location_ = fullFileName.substr(0, fullFileName.find_last_of('/'));
            fileName = fileName.erase(fileName.find_last_of('.'));
            info_.fileName_ = fileName;
            info_.displayName_ = projectElem.GetString("name").CString();
            info_.icon_ = projectElem.GetString("icon").CString();

            XMLElement folderElem{ projectElem.GetChild("folder") };

            while (folderElem)
            {
                const String path{ folderElem.GetValue() };
                info_.resourceFolders_.push_back(path.CString());
                cache()->AddResourceDir(AddTrailingSlash({ info_.location_.data() }) + path);

                folderElem = folderElem.GetNext("folder");
            }
            
            projectFile_ = file;

            return true;
        }
    }

    return false;
}

bool Project::read(const std::string& fileName)
{
    return read(Weaver::context()->GetSubsystem<ResourceCache>()
                ->GetResource<XMLFile>(fileName.data()));
}

bool Project::save()
{
    assert(!(info_.location_.empty() || info_.fileName_.empty()));

    const String fileName{ (info_.fileName_ + ".lkp").data() };

    if (!projectFile_)
    {
        projectFile_ = new XMLFile{ Weaver::context() };

    }
    else
    {
        if (GetFileName(projectFile_->GetName()) != fileName)
        {} // Delete file
    }

    projectFile_->SetName({ AddTrailingSlash(info_.location_.data()) + fileName });

    XMLElement projectElem{ projectFile_->GetOrCreateRoot("luckey").GetOrCreateChild("project") };
    projectElem.SetString("name", info_.displayName_.data());
    projectElem.SetString("icon", info_.icon_.data());


    for (const std::string& f : info_.resourceFolders_)
    {
        projectElem.CreateChild("folder").SetValue({ f.data() });
    }

    return projectFile_->SaveFile(projectFile_->GetName());
}

bool Project::usesFolder(const std::string& folder)
{
    for (const std::string& s: info_.resourceFolders_)
    {
        if (s == folder)
            return true;
    }

    return false;
}

void Project::addFolder(const std::string& folder)
{
    if (!usesFolder(folder))
        info_.resourceFolders_.push_back(folder);
}

void Project::removeFolder(const std::string& folder)
{
    if (usesFolder(folder))
    {
        for (auto f{ info_.resourceFolders_.begin() }; f != info_.resourceFolders_.end();)
        {
            if (*f == folder)
                f = info_.resourceFolders_.erase(f);
            else
                ++f;
        }
    }
}
