#ifndef PROJECT_H
#define PROJECT_H

#include <vector>
#include "../dry.h"

struct ProjectInfo {
    std::string icon_{};
    std::string location_{};
    std::string fileName_{};
    std::string displayName_{};
    std::vector<std::string> resourceFolders_{};
};

struct Project
{
    Project();
    Project(XMLFile* file);

    bool read(XMLFile* file);
    bool read(const std::string& fileName);
    bool save();

    bool usesFolder(const std::string& folder);
    void addFolder(const std::string& folder);
    void removeFolder(const std::string& folder);
    ResourceCache* cache() const { return context_->GetSubsystem<ResourceCache>(); }
    
    SharedPtr<XMLFile> projectFile_{};
    ProjectInfo info_{};

    SharedPtr<Context> context_;
};

#endif // PROJECT_H
