#ifndef PROJECTWIZARD_H
#define PROJECTWIZARD_H

#include <memory>

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTreeWidget>
#include <QDialogButtonBox>

#include "project.h"
#include <QDialog>

#define DEFAULT_ICONTIP "No icon set"

class ProjectWizard: public QDialog
{
    Q_OBJECT

public:
    ProjectWizard(QWidget* parent, Project* project = nullptr);

    std::weak_ptr<Project> project() { return project_; }

public slots:
    int exec() override;

private slots:
    void handleFileNameEdited();
    void handleDisplayNameEdited();
    void handleSelectionChanged();
    void toggleResourceFolder(bool checked);
    void setIcon(bool checked);

private:
    std::shared_ptr<Project> project_;
    std::string projectFolder_;
    std::string originalFileName_;

    QLabel* iconLabel_;
    QLabel* folderLabel_;
    QLineEdit* fileNameEdit_;
    QLineEdit* displayNameEdit_;
    QTreeWidget* projectTree_;
    QDialogButtonBox* buttonBox_;

    QAction* resourceAction_;
    QAction* iconAction_;

    bool selectRootFolder();
    void populateProjectTree();
    void growProjectTree(QTreeWidgetItem* parentItem);
    void addImages(QTreeWidgetItem* parentItem, const QString& parentPath);
    void addSubFolders(QTreeWidgetItem* parentItem, const QString& parentPath);
    void createInterface();
};

#endif // PROJECTWIZARD_H
