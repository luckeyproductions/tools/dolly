/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDebug>

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QUrl>
#include <QAction>
#include <QResizeEvent>
#include <QVBoxLayout>
#include <QUndoStack>

#include "view3d.h"
#include "project/projectwizard.h"
#include "weaver.h"

#include "qocoon.h"

Qocoon::Qocoon(Context* context): QMainWindow(), Object(context),
    project_{ nullptr },
    projectDependentActions_{},
    mainToolBar_{ nullptr },
    undoView_{ new QUndoView{ this } },
    view3d_{ new View3D{ context, this } },
    skeletonWidget_{ new SkeletonWidget{ this } },
    boneWidget_{ new BoneWidget{ this } },
    resourceBrowser_{ nullptr },
    model_{ nullptr },
    cellNode_{ nullptr },
    recentMenu_{ nullptr },
    hiddenDocks_{},
    actionTest_{ nullptr },
    actionUndo_{ nullptr },
    actionRedo_{ nullptr },
    actionFullView_{ new QAction{QIcon{ ":/FullView" }, "Fullscreen View", this } },
    actionDrawBones_{ nullptr },
    actionDrawCollisionShapes_{ nullptr },
    actionDrawConstraints_{ nullptr }
{
    context_->RegisterSubsystem(this);
    setWindowIcon(QIcon{ ":/Icon" });
    setCentralWidget(view3d_);
    setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

    undoView_->setEmptyLabel("Initial");
    undoView_->setMinimumHeight(040);
    undoView_->setStack(new QUndoStack{ this });
    QDockWidget* undoDockWidget{ new QDockWidget{ "History", this } };
    undoDockWidget->setObjectName("History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::LeftDockWidgetArea, undoDockWidget);

    createDockWidget(skeletonWidget_, Qt::LeftDockWidgetArea);
    createDockWidget(boneWidget_, Qt::LeftDockWidgetArea);
    connect(skeletonWidget_, SIGNAL(currentBoneChanged(Node*)), boneWidget_, SLOT(setBone(Node*)));
    connect(skeletonWidget_, SIGNAL(selectedBonesChanged()), view3d_, SLOT(refreshGarnish()));

    resourceBrowser_ = new ResourceBrowser{ context, this };
    createDockWidget(resourceBrowser_, Qt::BottomDockWidgetArea);

    setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    setCorner(Qt::BottomLeftCorner,  Qt::LeftDockWidgetArea);

    actionFullView_->setCheckable(true);
    actionFullView_->setShortcut(QKeySequence{ "F11" });
    addAction(actionFullView_);
    connect(actionFullView_, SIGNAL(triggered(bool)), this, SLOT(setFullViewEnabled(bool)));

    connect(this, SIGNAL(currentProjectChanged()), SLOT(updateAvailableActions()));
    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            view3d_, SLOT(updateView()));
    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            view3d_, SLOT(updateView()));

    createToolBar();
    createMenuBar();
    setStatusBar(new QStatusBar{ this });
    loadSettings();
    updateRecent();

    createScene();
    loadModel("Models/Dolly.mdl");
    statusBar()->showMessage("Welcome!");

    show();
}

Qocoon::~Qocoon()
{
    QSettings settings{};

    setFullViewEnabled(false);

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
}

void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
}

void Qocoon::createToolBar()
{
    mainToolBar_ = new QToolBar("Toolbar");
    mainToolBar_->setObjectName("MainToolbar");

    QString playName{ "Test" };
    actionTest_ = new QAction{ playName, this };
    actionTest_->setIcon(QIcon(":/" + playName ));
    actionTest_->setCheckable(true);
    mainToolBar_->addAction(actionTest_);
    connect(actionTest_, SIGNAL(triggered(bool)), this, SLOT(toggleRagdollTest(bool)));

    mainToolBar_->addSeparator();

    for (bool undo: { true, false })
    {
        QString  actionName{ QString(undo ? "Un" : "Re") + "do" };
        QAction* action{ new QAction(actionName, this) };
        action->setObjectName(actionName + "action");
        action->setIcon(QIcon(":/" + actionName ));
        action->setShortcut(QKeySequence("Ctrl+" + QString(undo ? "" : "Shift+") + "Z"));

        if (undo)
        {
            actionUndo_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(undo()));
        }
        else
        {
            actionRedo_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(redo()));
        }

        mainToolBar_->addAction(action);
    }

    updateUndoRedoAvailable();

    mainToolBar_->addSeparator();
    actionDrawBones_ = new QAction{ QIcon{ ":/Bone" }, "Draw bones", this };
    actionDrawBones_->setCheckable(true);
    actionDrawBones_->setChecked(true);
    mainToolBar_->addAction(actionDrawBones_);
    actionDrawCollisionShapes_ = new QAction{ QIcon{ ":/CollisionShape" }, "Draw collision shapes", this };
    actionDrawCollisionShapes_->setCheckable(true);
    mainToolBar_->addAction(actionDrawCollisionShapes_);
    actionDrawConstraints_ = new QAction{ QIcon{ ":/Constraint" }, "Draw constraints", this };
    actionDrawConstraints_->setCheckable(true);
    mainToolBar_->addAction(actionDrawConstraints_);

    connect(actionDrawBones_, SIGNAL(triggered(bool)), view3d_, SLOT(refreshGarnish()));
    for (QAction* debugAction: { actionDrawCollisionShapes_, actionDrawConstraints_ })
        connect(debugAction, SIGNAL(triggered(bool)), view3d_, SLOT(updateView()));

    addToolBar(mainToolBar_);
}

void Qocoon::createScene()
{
    Scene* scene{ new Scene{ context_ } };
    scene->SetUpdateEnabled(false);
    scene->CreateComponent<Octree>();
    scene->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);
    scene->CreateComponent<PhysicsWorld>()->setDebugMode(btIDebugDraw::DBG_DrawConstraints | btIDebugDraw::DBG_DrawConstraintLimits);
    view3d_->setScene(scene);

    Node* sunNode{ scene->CreateChild("Sun") };
    Light* sun{ sunNode->CreateComponent<Light>() };
    sunNode->SetPosition(10 * Vector3{ 0.55f, 2.3f, 1.7f });
    sunNode->LookAt(Vector3::ZERO);
    sun->SetCastShadows(true);
    sun->SetRange(100);

    Node* dollNode{ scene->CreateChild("Ragdoll") };
    model_ = dollNode->CreateComponent<AnimatedModel>();
    model_->SetCastShadows(true);

    cellNode_ = scene->CreateChild("Cell");
    StaticModel* cell{ cellNode_->CreateComponent<StaticModel>() };
    cell->SetModel(CACHE(Model)("Models/Cell.mdl"));
    cell->SetMaterial(CACHE(Material)("Materials/Cell.xml"));

    cellNode_->CreateComponent<RigidBody>();
    for (unsigned d{ 0u }; d < 6u; ++d)
    {
        Vector3 direction{};
        switch (d)
        {
        case 0u: direction =  Vector3::RIGHT;   break;
        case 1u: direction = -Vector3::RIGHT;   break;
        case 2u: direction =  Vector3::UP;      break;
        case 3u: direction = -Vector3::UP;      break;
        case 4u: direction =  Vector3::FORWARD; break;
        case 5u: direction = -Vector3::FORWARD; break;
        default: continue;
        }

        CollisionShape* wall{ cellNode_->CreateComponent<CollisionShape>() };
        wall->SetBox(Vector3::ONE * 3.f - 2.f * VectorAbs(direction), direction);
    }
}

void Qocoon::toggleRagdollTest(bool checked)
{
    QAction* playAction{ static_cast<QAction*>(sender()) };

    if (checked)
        playAction->setIcon(QIcon(":/TestIn"));
    else
        playAction->setIcon(QIcon(":/Test"));

    view3d_->setContinuousUpdate(checked);
    updateUndoRedoAvailable();
}

QMenu* Qocoon::createMenuBar()
{
    QMenu* fileMenu{ new QMenu{ "File" } };

    fileMenu->addAction(QIcon(":/New"),  "New Project...", this, SLOT(newProject()), QKeySequence("Ctrl+N"));
    fileMenu->addAction(QIcon(":/Open"), "Open...",        this, SLOT(open()),       QKeySequence("Ctrl+O"));
    recentMenu_ = fileMenu->addMenu("Recent");
    connect(this, SIGNAL(recentProjectsChanged()), SLOT(updateRecentProjectMenu()));

    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(), "Close",
                                                           this, SLOT(closeProject())));
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Save"), "Save Project...",
                                                           this, SLOT(savePrefab()), QKeySequence("Ctrl+S")));

    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Project"), "ProjectSettings...",
                                                           this, SLOT(editProject())));

    fileMenu->addSeparator();
    fileMenu->addAction(QIcon(":/Quit"), "Exit", this, SLOT(close()), QKeySequence("Ctrl+Q"));
    menuBar()->addMenu(fileMenu);

    QMenu* viewMenu{ new QMenu{ "View" } };
    viewMenu->addAction(actionFullView_);
    menuBar()->addMenu(viewMenu);

    QMenu* helpMenu{ new QMenu{ "Help" } };
    helpMenu->addAction(QIcon(":/About"), tr("About %1...").arg(Weaver::applicationDisplayName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);

    updateAvailableActions();

    return fileMenu;
}

void Qocoon::updateAvailableActions()
{
    for (QAction* action: projectDependentActions_)
        action->setEnabled(project_ != nullptr);
}

void Qocoon::updateRecentProjectMenu()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};

    recentMenu_->clear();

    for (const QString& recent: recentProjects)
    {
        QString displayName{ recent.left(recent.length() - (String(FILENAME_PROJECT).Length() + 1)) };
        recentMenu_->addAction(displayName, this, SLOT(openRecent()))->setObjectName(recent);
    }
}

void Qocoon::updateRecent()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};
//    QString currentProject{ toQString(projectLocation() + FILENAME_PROJECT) };

//    for (const QString& recent: recentProjects)
//    {
//        if (!QFileInfo(recent).exists() || currentProject == recent )
//            recentProjects.removeOne(recent);
//    }

//    if (project_)
//        recentProjects.push_front(currentProject);

//    while (recentProjects.size() > 10)
//        recentProjects.pop_back();

    settings.setValue("recentprojects", recentProjects);

    emit recentProjectsChanged(); /// Should not be emitted with no change
}

void Qocoon::open(QString filename) /// Open project, model or prefab
{
    if (filename.isEmpty()) // Pick a project through a file dialog
    {
        QString startPath{ /*project_ ? toQString(projectLocation())
                                    :*/ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };

        filename = QFileDialog::getOpenFileName(nullptr, tr("Open File"), startPath, "*.lkp *xml *.mdl");
        if (filename.isEmpty()) // Cancelled
            return;
    }

    if (filename.endsWith(".lkp"))
    {
        auto p{ std::make_shared<Project>() };

        if (p->read(filename.toStdString().c_str()))
            openProject(p);
    }
    else if (filename.endsWith(".xml"))
    {
        //Open prefab
        //Find/Create project
    }
    else if (filename.endsWith(".mdl"))
    {
        //Open model
        loadModel(toString(filename));
        //Find/Create project
        //Check for prefabs
    }
}

void Qocoon::openProject(std::shared_ptr<Project> project)
{
    if (project_ != project)
    {
        project_ = project;

        if (project != nullptr)
        {
            for (auto f: project_->info_.resourceFolders_)
            {
                GetSubsystem<ResourceCache>()->AddResourceDir(
                            AddTrailingSlash({ project_->info_.location_.data() }) + f.data());
            }

            statusBar()->showMessage({ ("Opened project " + project_->info_.displayName_).data() });
        }

        emit currentProjectChanged();
    }
}

bool Qocoon::loadModel(const String& filename)
{
    model_->SetModel(CACHE(Model)(filename));
    model_->SetMaterial(CACHE(Material)("Materials/VCol.xml"));

    unsigned lastSlash{ filename.FindLast('/') };

    if (lastSlash == String::NPOS)
        lastSlash = 0;
    else
        ++lastSlash;

    const String nodeName{ filename.Substring(
                           lastSlash,
                           filename.Length() - lastSlash - 4) };

    model_->GetNode()->SetName(nodeName);

    float scale{ 1.0f };

    for (int s{ 0 }; s < 3; ++s)
    {
        float size{};
        switch (s)
        {
        default:
        case 0: size = model_->GetBoundingBox().Size().x_;
        break;
        case 1: size = model_->GetBoundingBox().Size().y_;
        break;
        case 2: size = model_->GetBoundingBox().Size().z_;
        break;
        }

        size *= 2.3f;

        if (size > scale)
            scale = size;
    }

    cellNode_->SetScale(scale);
    cellNode_->SetPosition(Vector3::UP * scale * 0.5f);
    view3d_->jib()->lookAt(model_->GetWorldBoundingBox().Center());

    statusBar()->showMessage(toQString("Opened: " + filename));
    emit activeModelChanged(model_);

    for (const Bone& b: skeletonWidget_->bones())
    {
        Node* boneNode{ b.node_ };
        if (!boneNode)
            continue;

        RigidBody* rb{ boneNode->CreateComponent<RigidBody>() };
        rb->SetMass(.1f);
        rb->SetLinearDamping(0.05f);
        rb->SetAngularDamping(0.85f);
        rb->SetLinearRestThreshold(0.f);
        rb->SetAngularRestThreshold(2.5f);

        CollisionShape* shape{ boneNode->CreateComponent<CollisionShape>() };
        const float w{ b.boundingBox_.max_.x_ };
        shape->SetCapsule(Max(1.5f * w, .1f), b.boundingBox_.max_.y_, b.boundingBox_.Center(), Quaternion::IDENTITY);
    }

    for (const Bone& b: skeletonWidget_->bones())
    {
        Node* boneNode{ b.node_ };
        if (!boneNode)
            continue;
        Node* parentNode{ b.node_->GetParent() };
        if (!parentNode || skeletonWidget_->modelComponent()->GetSkeleton().GetBoneIndex(&b) == b.parentIndex_)
        {
            continue;
        }

        Constraint* constraint{ boneNode->CreateComponent<Constraint>() };
        constraint->SetConstraintType(CONSTRAINT_CONETWIST);
        constraint->SetDisableCollision(true);
        constraint->SetOtherBody(parentNode->GetComponent<RigidBody>());
        constraint->SetWorldPosition(boneNode->GetWorldPosition());

        const Vector3 axis{ boneNode->GetWorldDirection().CrossProduct(parentNode->GetWorldRight()) };
        constraint->SetAxis(axis);
        constraint->SetOtherAxis(boneNode->GetWorldUp().DotProduct(parentNode->GetWorldUp()) < 0.f ? -axis : axis);
        constraint->SetHighLimit(Vector2{ 45.f, 90.f });
        constraint->SetLowLimit(Vector2::ZERO);
    }

    return true;
}

int Qocoon::physicsDebugMode() const
{
    int debugMode{};
    if (actionDrawCollisionShapes_->isChecked())
        debugMode |= btIDebugDraw::DBG_DrawWireframe;
    if (actionDrawConstraints_->isChecked())
        debugMode |= btIDebugDraw::DBG_DrawConstraints | btIDebugDraw::DBG_DrawConstraintLimits;

    return debugMode;
}

bool Qocoon::drawBones() const
{
    return actionDrawBones_->isChecked();
}

bool Qocoon::savePrefab()
{
//    if (project_)
//        return project_->Save();
//    else
        return false;
}

void Qocoon::newProject()
{
    ProjectWizard* wizard{ new ProjectWizard{ this } };

    if (wizard->result() == QDialog::Accepted)
    {
        auto p{ wizard->project().lock() };

        if (project_ != p)
        {
            p->save();
            openProject(p);
        }
    }

    delete wizard;
}

void Qocoon::closeProject() //\\\ Should check for modified files
{
//    if (!project_)
//        return;

//    hololith_->setCurrentIndex(0);

//    project_->remove();
//    project_ = nullptr;

//    emit currentProjectChanged();
}

void Qocoon::editProject()
{
    ProjectWizard* wizard{ new ProjectWizard{ this, project_.get() } };
    auto p{ wizard->project().lock() };

    if (wizard->result() == QDialog::Accepted)
        openProject(p);

    delete wizard;
}

void Qocoon::setFullViewEnabled(bool enable)
{
    QSettings settings{};

    if (enable)
    {
        settings.setValue("geometry", saveGeometry());
        settings.setValue("state", saveState());

        for (QDockWidget* dock: findChildren<QDockWidget*>())
        {
            if (dock->isVisible())
                dock->setVisible(false);
        }

        statusBar()->setVisible(false);
        menuBar()->setVisible(false);
        mainToolBar_->setVisible(false);

        if (!isFullScreen())
            showFullScreen();

        actionFullView_->setShortcuts({ QKeySequence("F11"), QKeySequence("Esc") });
    }
    else if (isFullScreen())
    {
        if (statusBar())
            statusBar()->setVisible(true);

        if (menuBar())
            menuBar()->setVisible(true);

        showMaximized();

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());

        if (actionFullView_)
            actionFullView_->setShortcuts({ QKeySequence("F11") });
    }
}

void Qocoon::resizeEvent(QResizeEvent* event)
{
    if (static_cast<double>(event->size().width()) / event->size().height() > 1)
        setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    else
        setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
}

void Qocoon::about()
{
    QString aboutText{ tr("<p>Copyleft 🄯 2023 <a href=\"https://luckey.games\">LucKey Productions</a></b>"
                          "<p>You may use and redistribute this software under the terms "
                          "of the<br><a href=\"https://www.gnu.org/licenses/gpl.html\">"
                          "GNU General Public License Version 3</a>.</p>") };

    QDialog* aboutBox{ new QDialog(this) };

    aboutBox->setWindowTitle("About " + Weaver::applicationDisplayName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout() };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* DollyButton{ new QPushButton() };
    QPixmap DollyLogo{ ":/Icon" };
    DollyButton->setIcon(DollyLogo);
    DollyButton->setFlat(true);
    DollyButton->setMinimumSize(DollyLogo.width() * 04, DollyLogo.height());
    DollyButton->setIconSize(DollyLogo.size());
    DollyButton->setToolTip("https://gitlab.com/luckeyproductions/tools/dolly");
    DollyButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(DollyButton);
    aboutLayout->setAlignment(DollyButton, Qt::AlignHCenter);
    connect(DollyButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel(aboutText) };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout() };
    labelLayout->setContentsMargins(42, 34, 42, 12);
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox(QDialogButtonBox::Ok, aboutBox) };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}
void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl(qobject_cast<QPushButton*>(sender())->toolTip()));
}


void Qocoon::updateUndoRedoAvailable()
{
    actionUndo_->setEnabled(!actionTest_->isChecked() && undoView_->stack()->canUndo());
    actionRedo_->setEnabled(!actionTest_->isChecked() && undoView_->stack()->canRedo());
}

void Qocoon::undo()
{
    undoView_->stack()->undo();
}

void Qocoon::redo()
{
    undoView_->stack()->redo();
}
