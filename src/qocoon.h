/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOCOON_H
#define QOCOON_H

#include <memory>

#include <QMap>
#include <QMainWindow>
#include <QUndoView>

#include "project/project.h"
#include "skeletonwidget.h"
#include "bonewidget.h"
#include "resourcebrowser.h"

#include "drydockwidget.h"
#include "dry.h"


class View3D;

class Qocoon: public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(Qocoon, Object)

public:
    explicit Qocoon(Context* context);
    ~Qocoon();

    std::shared_ptr<Project> project() { return project_; }

    bool loadModel(const String& filename);

    QDockWidget* createDockWidget(QWidget* w, Qt::DockWidgetArea area)
    {
        QDockWidget* dockWidget{ new QDockWidget(w->objectName(), this) };
        dockWidget->setWidget(w);
        dockWidget->setObjectName(w->objectName() + "Dock");
        w->setParent(dockWidget);

        addDockWidget(area, dockWidget);
        return dockWidget;
    }

    int physicsDebugMode() const;
    bool drawBones() const;

signals:
    void activeModelChanged(AnimatedModel* model);

    void resourceFoldersChanged();
    void recentProjectsChanged();
    void currentProjectChanged();

public slots:
    void openRecent() { open(sender()->objectName()); }

protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void newProject();
    void open(QString filename = "");
    bool savePrefab();
    void editProject();
    void updateRecentProjectMenu();
    void updateAvailableActions();

    void setFullViewEnabled(bool enable);
    void about();
    void openUrl();
    void toggleRagdollTest(bool checked);
    void undo();
    void redo();

    void closeProject();

private:
    QMenu* createMenuBar();
    void createToolBar();
    void loadSettings();
    void updateRecent();
    void updateUndoRedoAvailable();

    void createScene();
    void openProject(std::shared_ptr<Project> project);

    std::shared_ptr<Project> project_;
    std::vector<QAction*> projectDependentActions_;
    QToolBar* mainToolBar_;
    QUndoView* undoView_;
    View3D* view3d_;
    SkeletonWidget* skeletonWidget_;
    BoneWidget* boneWidget_;
    ResourceBrowser* resourceBrowser_;
    AnimatedModel* model_;
    Node* cellNode_;

    QMenu* recentMenu_;
    std::vector<QDockWidget*> hiddenDocks_;
    QAction* actionTest_;
    QAction* actionUndo_;
    QAction* actionRedo_;
    QAction* actionFullView_;
    QAction* actionDrawBones_;
    QAction* actionDrawCollisionShapes_;
    QAction* actionDrawConstraints_;
};

#endif // QOCOON_H
