/* ManaWarg
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RESOURCEBROWSER_H
#define RESOURCEBROWSER_H

#include <QTreeWidget>
#include <QListWidget>
#include <QLabel>
#include <QPushButton>
#include <QTimer>

#include "view3d.h"

#include "drywidget.h"

enum PreviewType{ RT_Model, RT_Material, RT_Sound, RT_None};

class ResourcePreview: public QWidget, public Object
{
    Q_OBJECT
    DRY_OBJECT(ResourcePreview, Object);

public:
    ResourcePreview(Context* context, QWidget* parent = nullptr);
    void setResource(Resource* resource);

public slots:
    void changeResource(QTreeWidgetItem* item);
    void playSound();

private slots:
    void soundButtonToggled(bool checked);
    void soundFinished();

private:
    void createPreviewScene();
    void setModel(const QString& fileName);
    void setMaterial(const QString& fileName);
    void setSound(const QString& fileName);

    View3D* view3d_;
    QLabel* noPreviewLabel_;
    QPushButton* soundButton_;
    QTimer* soundTimer_;

    Resource* resource_;
    AnimatedModel* previewModel_;
    SoundSource* previewSoundSource_;
    PreviewType previewType_;
};

class ResourceBrowser: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(ResourceBrowser, DryWidget);

public:
    explicit ResourceBrowser(Context* context, QWidget* parent = nullptr);
    ~ResourceBrowser();

public slots:
    void refreshTreeView();
    void showEvent(QShowEvent*) override;

private slots:
    void updateFolderIcon(QTreeWidgetItem* item);
    void showBrowserMenu(const QPoint& pos);

private:
    void createTreeViewWidget();
    void createIconViewWidget();
    void buildTree(QTreeWidgetItem* treeItem);

    QWidget* browser_;
    QWidget* treeViewWidget_;
    QTreeWidget* treeWidget_;
    QWidget* iconViewWidget_;
    QListWidget* iconWidget_;
    FlipSplitter* splitter_;
    QWidget* resourcePreview_;
};

#endif // RESOURCEBROWSER_H
