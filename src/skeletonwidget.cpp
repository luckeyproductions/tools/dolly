/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>

#include "weaver.h"
#include "qocoon.h"

#include "skeletonwidget.h"

const Vector<Bone> SkeletonWidget::boneless_{};
PODVector<unsigned> SkeletonWidget::selected_{};

SkeletonWidget::SkeletonWidget(Qocoon* parent): DryWidget(parent->GetContext(), parent),
    nameLabel_{ new QLabel{ "Name" } },
    skeletonView_{ new QTreeWidget{} },
    model_{ nullptr }
{
    context_->RegisterSubsystem(this);
    setObjectName("Skeleton");

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setMargin(0);

    nameLabel_->setEnabled(false);
    mainLayout->addWidget(nameLabel_);

    skeletonView_->setSelectionMode(QAbstractItemView::ExtendedSelection);
    skeletonView_->setHeaderHidden(true);
    skeletonView_->setMinimumHeight(040);
    mainLayout->addWidget(skeletonView_);

    setLayout(mainLayout);

    connect(parent, SIGNAL(activeModelChanged(AnimatedModel*)), this, SLOT(updateModel(AnimatedModel*)));
    connect(skeletonView_, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
            this, SLOT(handleItemChanged(QTreeWidgetItem*)));
    connect(skeletonView_, SIGNAL(itemSelectionChanged()), this, SLOT(updateSelection()));
}

void SkeletonWidget::updateSelection()
{
    const QList<QTreeWidgetItem*> items{ skeletonView_->selectedItems() };
    const Vector<Bone>& allBones{ bones() };
    selected_.Clear();

    for (QTreeWidgetItem* boneItem: items)
    {
        for (const Bone& b: allBones)
        {
            const unsigned index{ model_->GetSkeleton().GetBoneIndex(&b) };
            if (boneItem->data(0, BoneIndex) == index)
                selected_.Push(index);
        }
    }

//    if (!items.isEmpty())
//    {
//        if (!items.contains(skeletonView_->currentItem()))
//            skeletonView_->setCurrentItem(items.back());
//    }

    emit selectedBonesChanged();
}

const Vector<Bone>& SkeletonWidget::bones() const
{
    if (!model_ || !model_->GetModel())
        return boneless_;

    return model_->GetSkeleton().GetBones();
}

PODVector<unsigned> SkeletonWidget::boneIndices(bool onlySelected) const
{
    PODVector<unsigned> indices{};
    if (model_ && model_->GetModel())
    {
        const Skeleton& skeleton{ model_->GetSkeleton() };
        for (const Bone& b: bones())
        {
            const unsigned index{ skeleton.GetBoneIndex(&b) };
            if (!onlySelected || selected_.Contains(index))
                indices.Push(index);
        }
    }

    return indices;
}

unsigned SkeletonWidget::currentBoneIndex() const
{
    if (skeletonView_)
    {
        QTreeWidgetItem* currentItem{ skeletonView_->currentItem() };
        if (currentItem)
            return currentItem->data(0, BoneIndex).toUInt();
    }

    return M_MAX_UNSIGNED;
}


void SkeletonWidget::updateModel(AnimatedModel* model)
{
    String modelName{ "Name" };

    skeletonView_->clear();

    if (model)
    {
        model_ = model;
        modelName = model_->GetNode()->GetName();

        for (const Bone& b: bones())
        {

            const String nodeName{ b.node_->GetName() };
            const unsigned boneIndex{ model_->GetSkeleton().GetBoneIndex(&b) };
//            Log::Write(LOG_INFO, "-------------------------------------");
//            Log::Write(LOG_INFO, "Bone " + String{ boneIndex } + " | " + nodeName + ":");
//            Log::Write(LOG_INFO, "Parent index: " + String{ b.parentIndex_ });
//            Log::Write(LOG_INFO, "Initial position: " + b.initialPosition_.ToString());
//            Log::Write(LOG_INFO, "Initial rotation: " + b.initialRotation_.ToString());
//            Log::Write(LOG_INFO, "Initial scale: "    + b.initialScale_.ToString());
//            Log::Write(LOG_INFO, "Position offset: "  + b.offsetMatrix_.Translation().ToString());
//            Log::Write(LOG_INFO, "Rotation offset: "  + b.offsetMatrix_.Rotation().ToString());
//            Log::Write(LOG_INFO, "Scale offset: "     + b.offsetMatrix_.Scale().ToString());

            if (boneIndex == b.parentIndex_)
            {
                QTreeWidgetItem* boneItem{ new QTreeWidgetItem{ { toQString(nodeName) } } };
                boneItem->setData(0, BoneIndex, boneIndex);

                skeletonView_->addTopLevelItem(boneItem);
                growBoneTree(boneItem);
            }
        }

        skeletonView_->expandAll();
    }
    else
    {
        model_ = nullptr;
    }

    setActiveBone(M_MAX_UNSIGNED);

    nameLabel_->setText(toQString(modelName));
    nameLabel_->setEnabled(model_);
}

void SkeletonWidget::growBoneTree(QTreeWidgetItem* parentItem)
{
    if (!model_ || !model_->GetModel())
        return;

    const unsigned parentIndex{ parentItem->data(0, BoneIndex).toUInt() };
    const Skeleton& skeleton{ model_->GetSkeleton() };

    for (const Bone& b: skeleton.GetBones())
    {
        const unsigned boneIndex{ skeleton.GetBoneIndex(&b) };

        if (boneIndex != parentIndex && b.parentIndex_ == parentIndex)
        {
            const String nodeName{ b.node_->GetName() };
            QTreeWidgetItem* boneItem{ new QTreeWidgetItem{ { toQString(nodeName) } } };
            boneItem->setData(0, BoneIndex, boneIndex);
            boneItem->setIcon(0, QIcon{":/Bone"} );

            parentItem->addChild(boneItem);
            growBoneTree(boneItem);
        }
    }
}

void SkeletonWidget::handleItemChanged(QTreeWidgetItem* current)
{
    if (current)
        setActiveBone(current->data(0, BoneIndex).toUInt());
}

void SkeletonWidget::setActiveBone(unsigned index)
{
    if (!skeletonView_->selectedItems().isEmpty() && index == M_MAX_UNSIGNED)
    {
        skeletonView_->setCurrentItem(nullptr);
        emit currentBoneChanged(nullptr);
        return;
    }

    if (index != M_MAX_UNSIGNED)
    {
        QTreeWidgetItemIterator it{ skeletonView_ };
        while (*it)
        {
            if ((*it)->data(0, BoneIndex).toUInt() == index)
            {
                (*it)->setSelected(true);
                emit currentBoneChanged(model_->GetSkeleton().GetBone(index)->node_);
                return;
            }

            ++it;
        }
    }
}
