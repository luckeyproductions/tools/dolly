/* Dolly
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SKELETONWIDGET_H
#define SKELETONWIDGET_H

#include <QLabel>
#include <QTreeWidget>

#include "drywidget.h"

class Qocoon;

enum DataRole{ BoneIndex = 0400, ParentIndex };

class SkeletonWidget: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(SkeletonWidget, DryWidget)

public:
    explicit SkeletonWidget(Qocoon* parent);

    AnimatedModel* modelComponent() const { return model_; }
    const Vector<Bone>& bones() const;
    PODVector<unsigned> boneIndices(bool onlySelected = false) const;
    unsigned currentBoneIndex() const;

signals:
    void currentBoneChanged(Node* node);
    void selectedBonesChanged();

public slots:
    void updateModel(AnimatedModel* model);
    void handleItemChanged(QTreeWidgetItem* current);
    void setActiveBone(unsigned index);

private slots:
    void updateSelection();

private:
    void growBoneTree(QTreeWidgetItem* boneItem);

    static const Vector<Bone> boneless_;
    static PODVector<unsigned> selected_;

    QLabel*    nameLabel_;
    QTreeWidget* skeletonView_;
    AnimatedModel* model_;
};

#endif // SKELETONWIDGET_H
